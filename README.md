# ScriviVeloce

## Where is?
[https://dcancani.gitlab.io/scriviveloce/](https://dcancani.gitlab.io/scriviveloce/)

## What is?
ScriviVeloce is a simple game to test your typing speed in Italian.
Every 24 hours the text will change and you can have a new challenge.

## License
Do what you want with it, I'll be happy.
